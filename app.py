import argparse
from pathlib import Path

from space_invaders.data_handler.scan import Scan
from space_invaders.data_handler.txt_stream import TXTDataSource
from space_invaders.invaders.alien import Alien, AlienType
from space_invaders.solver.direct_matching import DirectMatchingSolver


def parse_input() -> argparse.Namespace:
    """Parse input from command line."""
    parser = argparse.ArgumentParser("Space invaders detector", add_help=False)

    parser.add_argument(
        "--radar_scan",
        type=str,
        default="tests/radar_scans/radar_sample_0.txt",
        help="full path to the txt file with scan",
    )
    return parser.parse_args()


def get_scan(txt_data_handler: TXTDataSource, radar_scan_filename: Path) -> Scan:
    return Scan.from_filepath(
        filepath=radar_scan_filename.resolve(),
        reader=txt_data_handler,
    )


def get_alien_profiles(txt_data_handler: TXTDataSource) -> tuple[Alien, ...]:
    return (
        Alien.from_filepath(
            filepath=Path("space_invaders/invaders/profiles/spellcaster.txt"),
            reader=txt_data_handler,
            alien_type=AlienType.SPELLCASTER,
        ),
        Alien.from_filepath(
            filepath=Path("space_invaders/invaders/profiles/tank.txt"),
            reader=txt_data_handler,
            alien_type=AlienType.TANK,
        ),
    )


def main():
    args = parse_input()
    radar_scan_filename = Path(args.radar_scan)

    txt_data_handler = TXTDataSource()

    alien_profiles = get_alien_profiles(txt_data_handler)
    radar_scan = get_scan(txt_data_handler, radar_scan_filename)

    solver = DirectMatchingSolver()
    detections = solver.detect(
        targets=alien_profiles,
        scan=radar_scan,
    )
    print("Those who would not escape are:\n")
    print("\n".join(map(str, detections)))
    print("\nNOTE the visualisations are in project_root/runtime")


if __name__ == "__main__":
    main()
