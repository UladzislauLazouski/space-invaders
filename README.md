# Space invaders are upon us!
App takes a radar sample and reveals possible locations of those pesky invaders.


## Problem statement
### Requirements:
- This is all about ASCII patterns
- This is all about image detection

### Caveats:
- The noise in the radar can be either false positives or false negatives
- Think of edge cases ... pun intended ;)


![](/git_images/detection_sample.jpeg)

### Installation
This package requires Python >=3.9.
[Poetry](https://python-poetry.org/docs/) is used as a dependency management tool.


```bash
poetry install  # create new virtual env and install specified dependencies inside
poetry shell  # activate virtual environment
```

To avoid the development dependencies installation add the --no-dev option.
```bash
poetry install --no-dev
```

### Contibution
To keep codebase populated with a maintainable, well-documented and tested code pre-commit hooks are configured. Those help to keep source code is automatically checked for errors and style adhesion by utilizing linting and formatting engines.

Pre-commit documentation is available at [Link](https://pre-commit.com/)

Run pre-commit install to set up the git hook scripts
```bash
pre-commit install
```

### Testing
Pytest is used as a core testing framework and recommended options are added in pyproject.toml
To execute tests from the root of the repo one should:

```bash
poetry run pytest
```