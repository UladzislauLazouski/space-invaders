from datetime import datetime
from pathlib import Path

import matplotlib.patches as patches
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.axes import Axes


def get_datetime_tag() -> str:
    return datetime.now().strftime("%Y-%m-%d_%H_%M.%f")[:-3]


def add_detections_and_bboxes(
    search_template: np.ndarray,
    coords: np.ndarray,
    ax_orig: Axes,
):
    for y, x in coords:
        ax_orig.plot(x, y, "ro")
        h, w = search_template.shape
        yr, xr = int(y - h / 2), int(x - w / 2)

        ax_orig.add_patch(
            patches.Rectangle(
                (xr, yr), w, h, linewidth=1, edgecolor="r", facecolor="none"
            )
        )


def visualize(
    search_space: np.ndarray,
    search_template: np.ndarray,
    activation_map: np.ndarray,
    coords: np.ndarray,
    method_name: str,
):
    fig, (ax_orig, ax_template, ax_corr) = plt.subplots(3, 1, figsize=(6, 12))
    ax_orig.imshow(search_space, cmap="gray")
    ax_orig.set_title("Original")

    ax_template.imshow(search_template, cmap="gray")
    ax_template.set_title("Template")

    ax_corr.imshow(activation_map, cmap="gray")
    ax_corr.set_title(f"Activation map for {method_name}")

    add_detections_and_bboxes(
        search_template=search_template,
        coords=coords,
        ax_orig=ax_orig,
    )

    for ax in (ax_orig, ax_template, ax_corr):
        ax.set_axis_off()

    plt.tight_layout()

    tag = get_datetime_tag()
    save_dir = Path("runtime")
    save_dir.mkdir(parents=True, exist_ok=True)
    plt.savefig(save_dir / f"{method_name}_{tag}.png")
