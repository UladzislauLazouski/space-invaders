from dataclasses import dataclass
from enum import Enum, auto
from typing import Optional

import numpy as np

from space_invaders.examinator.weighted import WeightedActivationMapExaminator
from space_invaders.solver.interface import Solver


class Edge(Enum):
    LEFT = auto()
    RIGHT = auto()
    TOP = auto()
    BOTTOM = auto()


@dataclass(frozen=True)
class EdgeTransformationConfig:
    """Transofrmations need to be applied to each edge"""

    name: Edge
    rot_times: int
    min_overlap: float
    padding: int


class ScanExamiinatorConfig:
    min_overlap_vertical: float = 0.5
    min_overlap_horizontal: float = 0.5


class ActivationMapCalculator:
    def __init__(
        self,
        scan: np.ndarray,
        kernel: np.ndarray,
        cfg: Optional[ScanExamiinatorConfig] = None,
    ):
        self.scan = scan
        self.kernel = kernel
        self.cfg = cfg or ScanExamiinatorConfig()

        self.kernel_h, self.kernel_w = self.kernel.shape
        self.scan_h, self.scan_w = self.scan.shape

    def calculate(self):
        """
        Calculate activation map in center piece and borders.
        Center piece is matched and normalized to full kernel overlap.
        Borders are matched and normalized to partial kernel overlap.
        """
        center_map = self.calculate_at_center()
        edges_map = self.calculate_at_edges()
        return np.maximum(center_map, edges_map)

    def calculate_at_center(self) -> np.ndarray:
        """
        Calculate activation in center piece, with full kernel overlap.
        Normalized to full kernel size.
        """
        # NOTE the current implementation could be quicken,
        # but for the sake of readability it is not
        # Possible techniques - index system + inner piecewise operations

        # Activation map
        a_map = np.zeros_like((self.scan), dtype=np.float32)

        # Max numer of similar pixels
        max_pix_num = self.kernel_h * self.kernel_w

        # Scan within unpadded area
        step_h = self.scan_h - self.kernel_h
        step_w = self.scan_w - self.kernel_w

        for i in range(step_h):
            for j in range(step_w):
                cut = self.scan[i : i + self.kernel_h, j : j + self.kernel_w]
                ii, jj = int(i + self.kernel_h / 2), int(j + self.kernel_w / 2)
                a_map[ii, jj] = np.sum(cut == self.kernel) / max_pix_num
        return a_map

    def calculate_at_top_edge(
        self,
        scan: np.ndarray,
        kernel: np.ndarray,
        min_overlap: float,
        padding: int,
    ) -> np.ndarray:
        """
        Calculate activation in top border zone, with partial kernel overlap.
        Normalized to subkernel overlap.
        """
        # NOTE the current implementation could be quicken,
        # but for the sake of readability it is not
        # Possible techniques - index system + inner piecewise operations

        # Activation map
        a_map = np.zeros_like((scan), dtype=np.float32)

        kernel_h, kernel_w = kernel.shape
        filter_reduction = int(kernel_h * (1 - min_overlap))

        for i in range(1, filter_reduction + 1):
            for j in range(scan.shape[1] - kernel_w):
                # Slice the original kernel to emulate object partially in scene
                subkernel = kernel[i:, :]
                subkernel_h, subkernel_w = subkernel.shape

                # Max numer of similar pixels
                max_pix_num = subkernel_h * subkernel_w

                # Part of the scan that overlap with subkernel
                cut = scan[padding : padding + subkernel_h, j : j + subkernel_w]

                # Shifted coordinates to compensate for object size
                ii = int(padding - kernel_h / 2 + subkernel_h)
                jj = int(j + kernel_w / 2)

                # Calculated activation for shifted center
                a_map[ii, jj] = np.sum(cut == subkernel) / max_pix_num

        return a_map

    @property
    def edges_transforms(self) -> list[EdgeTransformationConfig]:
        return [
            EdgeTransformationConfig(
                name=Edge.TOP,
                rot_times=0,
                padding=self.kernel_h,
                min_overlap=self.cfg.min_overlap_vertical,
            ),
            EdgeTransformationConfig(
                name=Edge.RIGHT,
                rot_times=1,
                padding=self.kernel_w,
                min_overlap=self.cfg.min_overlap_horizontal,
            ),
            EdgeTransformationConfig(
                name=Edge.BOTTOM,
                rot_times=2,
                padding=self.kernel_h,
                min_overlap=self.cfg.min_overlap_vertical,
            ),
            EdgeTransformationConfig(
                name=Edge.LEFT,
                rot_times=3,
                padding=self.kernel_w,
                min_overlap=self.cfg.min_overlap_horizontal,
            ),
        ]

    def calculate_at_edges(self) -> np.ndarray:
        """
        Calculate activation in border zones, with partial kernel overlap.
        Normalized to subkernel overlap.
        """
        # Combined activation map over all edges
        a_map = np.zeros_like((self.scan), dtype=np.float32)

        # Rotate each edge such it appears "as top edge"
        for trf in self.edges_transforms:
            # Rotate search space and kernel
            kernel = np.rot90(self.kernel, trf.rot_times)
            scan = np.rot90(self.scan, trf.rot_times)

            # Calculate edge activations
            edge_map = self.calculate_at_top_edge(
                scan=scan,
                kernel=kernel,
                min_overlap=trf.min_overlap,
                padding=trf.padding,
            )

            # Rotate activation map to original coordinate system
            rotated_edge_map = np.rot90(edge_map, trf.rot_times, axes=(1, 0))

            # NOTE Take maximum as activation map appear smoother in corners
            a_map = np.maximum(a_map, rotated_edge_map)

        return a_map


class DirectMatchingSolver(Solver):
    def get_activation_map(
        self, search_space: np.ndarray, search_template: np.ndarray
    ) -> np.ndarray:
        """Calculate activation map for provided template"""
        a_map_cal = ActivationMapCalculator(scan=search_space, kernel=search_template)
        return a_map_cal.calculate()

    def extract_detections(
        self,
        activation_map: np.ndarray,
        search_template: Optional[np.ndarray] = None,
    ) -> np.ndarray:
        """Examine activation map and find object positions"""
        if search_template is None:
            raise ValueError(
                "Search template is used for Examination of Direct Matching Activation match. Please proivide it "
            )
        amap_exam = WeightedActivationMapExaminator(
            activation_map=activation_map, kernel=search_template
        )
        return amap_exam.examine()

    @property
    def solver_name(self) -> str:
        return "DirectMatching"
