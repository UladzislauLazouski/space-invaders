from typing import Optional

import numpy as np
from scipy.signal import correlate2d

from space_invaders.examinator.macro import MacroAtivationMapExaminator
from space_invaders.solver.interface import Solver


class CrossCorelationSolver(Solver):
    def __init__(self, threshold: float = 0.99):
        self.threshold = threshold

    def get_activation_map(
        self, search_space: np.ndarray, search_template: np.ndarray
    ) -> np.ndarray:
        return correlate2d(search_space, search_template, boundary="symm", mode="same")

    def extract_detections(
        self,
        activation_map: np.ndarray,
        search_template: Optional[np.ndarray] = None,
    ) -> np.ndarray:
        """Examine activation map and find object positions"""
        amap_exam = MacroAtivationMapExaminator(
            activation_map=activation_map,
            threshold=self.threshold,
        )
        return amap_exam.examine()

    @property
    def solver_name(self) -> str:
        return "CrossCorrelation"
