from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Optional, TypeVar

import numpy as np

from space_invaders.data_handler.scan import Padding, Scan, ScanPreProcessor
from space_invaders.invaders.alien import Alien, AlienType
from space_invaders.utils.visuals import visualize

T = TypeVar("T")


def flatten(ll: list[list[T]]) -> list[T]:
    return [elem for sublist in ll for elem in sublist]


@dataclass(frozen=True)
class Detection:
    """Represent 2D detection in the provided scan coordinate system"""

    x: int
    y: int
    alien_type: AlienType

    def __repr__(self) -> str:
        return f"{self.alien_type} at {self.x}, {self.y}"


@dataclass(frozen=True)
class ActivationMap:
    map: np.ndarray
    coords: np.ndarray


def detections_from_coords(
    coords: np.ndarray, alien_type: AlienType
) -> list[Detection]:
    return [Detection(x=x, y=y, alien_type=alien_type) for y, x in coords]


class Solver(ABC):
    @abstractmethod
    def get_activation_map(
        self, search_space: np.ndarray, search_template: np.ndarray
    ) -> np.ndarray:
        ...

    @abstractmethod
    def extract_detections(
        self,
        activation_map: np.ndarray,
        search_template: Optional[np.ndarray] = None,
    ) -> np.ndarray:
        ...

    @property
    @abstractmethod
    def solver_name(self) -> str:
        ...

    def detect(self, targets: list[Alien], scan: Scan) -> list[Detection]:
        detections = [self.check_for_target(t, scan) for t in targets]
        return flatten(detections)

    def unpad_coordinates(
        self, padded_cords: np.ndarray, padding: Padding
    ) -> np.ndarray:
        coords = [(y - padding.h, x - padding.w) for y, x in padded_cords]
        return np.array(coords)

    def check_for_target(self, target: Alien, unpadded_scan: Scan) -> list[Detection]:

        scan, padding = ScanPreProcessor.pad_scan_to_targets(
            targets=[target], scan=unpadded_scan
        )

        activation_map = self.get_activation_map(
            search_space=scan.content,
            search_template=target.profile.content,
        )
        padded_cords = self.extract_detections(
            activation_map=activation_map,
            search_template=target.profile.content,
        )

        coords = self.unpad_coordinates(padded_cords, padding)

        visualize(
            search_space=unpadded_scan.content,
            search_template=target.profile.content,
            activation_map=activation_map,
            coords=coords,
            method_name=self.solver_name,
        )
        return detections_from_coords(coords, target.alien_type)
