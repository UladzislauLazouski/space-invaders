from pathlib import Path

import numpy as np

from space_invaders.data_handler.file_datasource import (
    FileDataSource,
    WrongExtensionError,
)


class TXTDataSource(FileDataSource):
    APPROVED_FILE_EXTENSIONS = [".txt"]

    def check_extension(self, filepath: Path) -> bool:
        if filepath.suffix not in TXTDataSource.APPROVED_FILE_EXTENSIONS:
            raise WrongExtensionError(
                (
                    f"file extenstion should be one of "
                    f"{TXTDataSource.APPROVED_FILE_EXTENSIONS} "
                    f"but {filepath} was requested"
                )
            )
        return True

    def parse_file(self, filepath: Path) -> np.ndarray:
        with open(filepath) as f:
            lines = [self.map_data(line.strip()) for line in f]
        return np.array(lines, dtype=np.int8)
