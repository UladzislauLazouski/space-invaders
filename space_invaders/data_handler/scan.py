from dataclasses import dataclass
from pathlib import Path
from typing import Optional, Protocol

import numpy as np

from space_invaders.invaders.alien import Alien


class ScanReader(Protocol):
    def read_file(self, filepath: Path) -> np.ndarray:
        ...


@dataclass(frozen=True)
class Padding:
    w: int
    h: int


@dataclass(frozen=True)
class Scan:
    """Radar scan"""

    content: np.ndarray
    filepath: Optional[Path] = None

    @property
    def w(self) -> int:
        return self.content.shape[1]

    @property
    def h(self) -> int:
        return self.content.shape[0]

    @classmethod
    def from_filepath(cls, filepath: Path, reader: ScanReader):
        return Scan(
            content=reader.read_file(filepath),
            filepath=filepath,
        )


class ScanPreProcessor:
    @staticmethod
    def pad_scan_to_targets(targets: list[Alien], scan: Scan) -> tuple[Scan, Padding]:
        padding = ScanPreProcessor.least_common_padding(targets)
        return ScanPreProcessor.pad_scan(scan, padding), padding

    @staticmethod
    def pad_scan(scan: Scan, padding: Padding) -> Scan:
        pad_footage = (
            (padding.h, padding.h),
            (padding.w, padding.w),
        )
        padded = np.pad(scan.content, pad_footage, constant_values=0)
        return Scan(content=padded)

    @staticmethod
    def unppad_scan(scan: Scan, padding: Padding) -> Scan:
        h, w = padding.h, padding.w
        unpadded = scan.content[h:-h, w:-w]
        return Scan(unpadded)

    @staticmethod
    def least_common_padding(targets: list[Alien]) -> Padding:
        return Padding(
            w=max(t.profile.w for t in targets),
            h=max(t.profile.h for t in targets),
        )
