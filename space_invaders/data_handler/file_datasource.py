from abc import ABC, abstractmethod
from functools import partial
from pathlib import Path
from typing import Callable

import numpy as np

FileCheck = Callable[[Path], bool]


class WrongExtensionError(ValueError):
    pass


class FileDataSource(ABC):
    def check_data(self, filepath):
        """Run check on the data source"""
        checks = [partial(func, filepath=filepath) for func in self.checks]
        return all(func() for func in checks)

    @property
    def checks(self) -> tuple[FileCheck, ...]:
        return (self.check_extension, self.check_file_exists)

    def check_file_exists(self, filepath: Path) -> bool:
        if not filepath.resolve().exists():
            raise FileExistsError(f"filepath {filepath} do not exist")
        return True

    @abstractmethod
    def check_extension(self, filepath: Path) -> bool:
        ...

    @abstractmethod
    def parse_file(self, filepath: Path) -> np.ndarray:
        ...

    def read_file(self, filepath: Path) -> np.ndarray:
        self.check_data(filepath)
        return self.parse_file(filepath)

    def map_data(self, line: str) -> np.ndarray:
        """Map a line of ASCII string into np.int8 array"""
        return np.array(list(map(ord, line)), dtype=np.int8)
