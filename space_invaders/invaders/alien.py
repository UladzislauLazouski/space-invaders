from dataclasses import dataclass
from enum import Enum, auto
from pathlib import Path
from typing import Protocol

import numpy as np


class AlienReader(Protocol):
    def read_file(self, filepath: Path) -> np.ndarray:
        ...


class AlienType(Enum):
    TANK = auto()
    SPELLCASTER = auto()
    HEADCRAB = auto()
    GMAN = auto()


@dataclass(frozen=True)
class AlienProfile:
    """Alien definition"""

    content: np.ndarray

    @property
    def w(self) -> int:
        return self.content.shape[1]

    @property
    def h(self) -> int:
        return self.content.shape[0]


@dataclass(frozen=True)
class Alien:
    """Alien entity system is searching for"""

    filepath: Path
    alien_type: AlienType
    profile: AlienProfile

    @classmethod
    def from_filepath(
        cls,
        filepath: Path,
        reader: AlienReader,
        alien_type: AlienType,
    ):
        return Alien(
            alien_type=alien_type,
            profile=AlienProfile(content=reader.read_file(filepath)),
            filepath=filepath,
        )
