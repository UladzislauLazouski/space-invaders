from abc import abstractmethod

import numpy as np


class ActivationMapExaminator:
    @abstractmethod
    def examine(self) -> np.ndarray:
        ...
