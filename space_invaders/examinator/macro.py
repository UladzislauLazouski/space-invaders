import numpy as np

from space_invaders.examinator.interface import ActivationMapExaminator


class MacroAtivationMapExaminator(ActivationMapExaminator):
    def __init__(
        self,
        activation_map: np.ndarray,
        threshold: float,
    ):
        self.activation_map = activation_map
        self.threshold = threshold

    def examine(self) -> np.ndarray:
        """
        Examine all parts of the activation map and
        extract coordinates of detected objects
        """
        max_val = self.activation_map.max() * self.threshold
        return np.argwhere(self.activation_map > max_val)
