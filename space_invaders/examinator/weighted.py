from typing import Optional

import numpy as np

from space_invaders.examinator.interface import ActivationMapExaminator


class WeightedActivationMapExaminatorConfig:
    center_confidence: float = 0.75
    edge_confidence: float = 0.8
    corner_confidence: float = 0.65


class WeightedActivationMapExaminator(ActivationMapExaminator):
    def __init__(
        self,
        activation_map: np.ndarray,
        kernel: np.ndarray,
        cfg: Optional[WeightedActivationMapExaminatorConfig] = None,
    ):
        self.kernel = kernel
        self.activation_map = activation_map
        self.cfg = cfg or WeightedActivationMapExaminatorConfig()

    def examine(self) -> np.ndarray:
        """
        Examine all parts of the activation map and
        extract coordinates of detected objects
        """
        # NOTE not properly scallable yet
        coords = np.vstack(
            (
                self.examine_center(),
                self.examine_edges(),
                self.examine_corners(),
            )
        )
        return np.unique(coords, axis=0)

    def examine_center(self) -> np.ndarray:
        """Examine center piece of the padded scan."""
        return np.argwhere(self.activation_map > self.cfg.center_confidence)

    def examine_edges(self) -> np.ndarray:
        """
        Examine edges of the padded scan.
        Edges width is temporary set to search kernel size.
        """

        # NOTE scan is padded and we ignore the pad zone
        pad_h, pad_w = self.kernel.shape
        h, w = self.kernel.shape

        border_mask = np.zeros_like(self.activation_map, dtype=bool)
        border_mask[pad_h : pad_h + h, :] = 1  # left
        border_mask[-h - pad_h : -pad_h, :] = 1  # right
        border_mask[:, pad_w : pad_w + w] = 1  # top
        border_mask[:, -w - pad_w : -pad_w] = 1  # bottom

        masked = self.activation_map * border_mask
        return np.argwhere(masked > self.cfg.edge_confidence)

    def examine_corners(self) -> np.ndarray:
        """
        Examine corners of the padded scan.
        Corner size is temporary set to search kernel size.
        """

        # NOTE scan is padded and we ignore the pad zone
        pad_h, pad_w = self.kernel.shape
        h, w = self.kernel.shape

        corner_mask = np.zeros_like(self.activation_map, dtype=bool)
        corner_mask[pad_h : pad_h + h, pad_w : pad_w + w] = 1  # top-left
        corner_mask[pad_h : pad_h + h, -w - pad_w : -pad_w] = 1  # top-right
        corner_mask[-h - pad_h : -pad_h, pad_w : pad_w + w] = 1  # bottom-left
        corner_mask[-h - pad_h : pad_h, -w - pad_w : -pad_w] = 1  # bottom-right

        masked = self.activation_map * corner_mask
        return np.argwhere(masked > self.cfg.corner_confidence)
