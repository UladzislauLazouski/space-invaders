from space_invaders.invaders.alien import AlienType
from space_invaders.solver.interface import Detection


def _default_tank_detections() -> tuple[Detection, ...]:
    """Expected detection of Tank Aliens in radar_sample_0.txt"""
    return (
        Detection(x=15, y=3, alien_type=AlienType.TANK),
        Detection(x=79, y=5, alien_type=AlienType.TANK),
        Detection(x=90, y=16, alien_type=AlienType.TANK),
        Detection(x=65, y=17, alien_type=AlienType.TANK),
    )


def _default_spellcaster_detections() -> tuple[Detection, ...]:
    """Expected detection of Spellcaster Aliens in radar_sample_0.txt"""
    return (
        Detection(x=22, y=3, alien_type=AlienType.SPELLCASTER),
        Detection(x=46, y=4, alien_type=AlienType.SPELLCASTER),
        Detection(x=39, y=19, alien_type=AlienType.SPELLCASTER),
        Detection(x=20, y=32, alien_type=AlienType.SPELLCASTER),
        Detection(x=86, y=45, alien_type=AlienType.SPELLCASTER),
        Detection(x=20, y=48, alien_type=AlienType.SPELLCASTER),
    )
