from pathlib import Path

import numpy as np
import pytest

from space_invaders.data_handler.file_datasource import WrongExtensionError
from space_invaders.invaders.alien import Alien, AlienType
from space_invaders.solver.interface import Scan


def test_txt_reader(txt_data_handler, tank_filename):
    content = txt_data_handler.read_file(tank_filename)

    assert type(content) is np.ndarray
    assert content.sum() == 6996


def test_txt_reader_data_mapping(txt_data_handler):
    ascii_line = "---oo---"
    line = txt_data_handler.map_data(ascii_line)

    assert type(line) is np.ndarray
    assert type(line[0]) is np.int8
    assert line.sum() == 492


def test_txt_checks(txt_data_handler):
    with pytest.raises(WrongExtensionError):
        _ = txt_data_handler.read_file(Path("wrong_exntension.csv"))

    with pytest.raises(FileExistsError):
        _ = txt_data_handler.read_file(Path("do_not_exist.txt"))


def test_read_alien_tank(txt_data_handler, tank_filename):
    alien = Alien.from_filepath(
        filepath=tank_filename,
        reader=txt_data_handler,
        alien_type=AlienType.TANK,
    )
    assert alien.profile.content.shape == (8, 11)
    assert alien.profile.content.sum() == 6996


def test_read_alien_spellcaster(txt_data_handler, spellcaster_filename):
    alien = Alien.from_filepath(
        filepath=spellcaster_filename,
        reader=txt_data_handler,
        alien_type=AlienType.SPELLCASTER,
    )
    assert alien.profile.content.shape == (8, 8)
    assert alien.profile.h, alien.profile.h == (8, 8)
    assert alien.profile.content.sum() == 5256


def test_read_scan(txt_data_handler, radar_scan_filename):
    scan = Scan.from_filepath(
        filepath=radar_scan_filename,
        reader=txt_data_handler,
    )

    assert type(scan) is Scan
    assert scan.content.shape == (50, 100)
    assert scan.h, scan.w == (50, 100)
    assert scan.content.sum() == 295918
