from pathlib import Path

import pytest

from space_invaders.data_handler.scan import Scan
from space_invaders.data_handler.txt_stream import TXTDataSource
from space_invaders.invaders.alien import Alien, AlienType


@pytest.fixture
def txt_data_handler():
    yield TXTDataSource()


@pytest.fixture
def spellcaster_filename():
    yield Path("space_invaders/invaders/profiles/spellcaster.txt")


@pytest.fixture
def tank_filename():
    yield Path("space_invaders/invaders/profiles/tank.txt")


@pytest.fixture
def radar_scan_filename():
    yield Path("tests/radar_scans/radar_sample_0.txt")


@pytest.fixture
def scan_sample(txt_data_handler, radar_scan_filename):
    yield Scan.from_filepath(
        filepath=radar_scan_filename,
        reader=txt_data_handler,
    )


@pytest.fixture
def spellcaster(txt_data_handler, spellcaster_filename):
    yield Alien.from_filepath(
        filepath=spellcaster_filename,
        reader=txt_data_handler,
        alien_type=AlienType.SPELLCASTER,
    )


@pytest.fixture
def tank(txt_data_handler, tank_filename):
    yield Alien.from_filepath(
        filepath=tank_filename,
        reader=txt_data_handler,
        alien_type=AlienType.TANK,
    )
