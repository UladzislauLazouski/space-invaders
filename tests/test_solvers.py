from functools import partial

from space_invaders.invaders.alien import AlienType
from space_invaders.solver.conv_solver import ConvSolver
from space_invaders.solver.cross_corr_solver import CrossCorelationSolver
from space_invaders.solver.direct_matching import DirectMatchingSolver
from tests.expected_detection import (
    _default_spellcaster_detections,
    _default_tank_detections,
)


def check_equal_containers(a, b):
    assert len(a) == len(b)

    sorted_by_x = partial(sorted, key=lambda elem: elem.x)
    assert sorted_by_x(a) == sorted_by_x(b)


def test_tank_detection_with_conv(scan_sample, tank):
    solver = ConvSolver()
    detections = solver.detect([tank], scan_sample)

    assert len(detections) == 74  # currently over sensitive to noise
    assert all(x.alien_type == AlienType.TANK for x in detections)


def test_spellcaster_detection_with_conv(scan_sample, spellcaster):
    solver = ConvSolver()
    detections = solver.detect([spellcaster], scan_sample)

    assert len(detections) == 28  # currently over sensitive to noise
    assert all(x.alien_type == AlienType.SPELLCASTER for x in detections)


def test_tank_detection_with_cross_corr(scan_sample, tank):
    solver = CrossCorelationSolver()
    detections = solver.detect([tank], scan_sample)

    assert len(detections) == 70  # currently over sensitive to noise
    assert all(x.alien_type == AlienType.TANK for x in detections)


def test_spellcaster_detection_with_cross_corr(scan_sample, spellcaster):
    solver = CrossCorelationSolver()
    detections = solver.detect([spellcaster], scan_sample)

    assert len(detections) == 26  # currently over sensitive to noise
    assert all(x.alien_type == AlienType.SPELLCASTER for x in detections)


def test_tank_detection_with_direct_match(scan_sample, tank):
    solver = DirectMatchingSolver()
    detections = solver.detect([tank], scan_sample)
    expected = _default_tank_detections()

    check_equal_containers(detections, expected)
    assert all(x.alien_type == AlienType.TANK for x in detections)


def test_spellcaster_detection_with_direct_match(scan_sample, spellcaster):
    solver = DirectMatchingSolver()
    detections = solver.detect([spellcaster], scan_sample)
    expected = _default_spellcaster_detections()

    check_equal_containers(detections, expected)
    assert all(x.alien_type == AlienType.SPELLCASTER for x in detections)
