from space_invaders.data_handler.scan import Padding, ScanPreProcessor


def test_scan_padding(scan_sample):
    unpadded_scan = scan_sample

    padding = Padding(w=5, h=19)
    padded_scan = ScanPreProcessor.pad_scan(unpadded_scan, padding)

    assert padded_scan.content.sum() == unpadded_scan.content.sum()
    assert padded_scan.content.shape == (88, 110)
    assert unpadded_scan.content.shape == (50, 100)


def test_least_common_padding(spellcaster, tank):
    padding = ScanPreProcessor.least_common_padding([spellcaster, tank])

    assert padding.w == 11
    assert padding.h == 8


def test_pad_scan_to_various_targets(scan_sample, spellcaster, tank):
    aliens = [spellcaster, tank]
    padded, padding = ScanPreProcessor.pad_scan_to_targets(aliens, scan_sample)

    assert padded.w == 122
    assert padded.h == 66
    assert padding.w == 11
    assert padding.h == 8


def test_pad_upnpad(scan_sample):
    original_scan = scan_sample

    padding = Padding(w=5, h=19)
    padded_scan = ScanPreProcessor.pad_scan(original_scan, padding)
    unpadded_scan = ScanPreProcessor.unppad_scan(padded_scan, padding)

    assert unpadded_scan.w == original_scan.w
    assert unpadded_scan.h == original_scan.h
    assert unpadded_scan.content.sum() == original_scan.content.sum()
    assert unpadded_scan.content.sum() == padded_scan.content.sum()
